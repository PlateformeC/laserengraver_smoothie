lxml==3.8.0
numpy==1.13.1
Pillow==5.3.0
py==1.4.34
pytest==3.2.2
