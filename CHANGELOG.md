# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [0.07-beta2] - 2018-11-26
### Added
- add acceleration change for raster engraving
- add margin for raster engraving
- add verbose header

### Changed
- remove 'G92 Z0' in gcode header and 'G0 Z0' in gcode footer
- handle layer without label 
- use selected images for raster if images are selected (all present images otherwise)
- lighter raster gcode
  - remove a lot of duplicated lines
  - remove redundant command (like speed)
- encoding issue during gcode writing (now handling special character in layer names)

## [0.06] - 2018-10-25
### Added
- add debug trace for raster
- warn the user that he is going to perform a cut before engraving
- add RASTER layer: if at least one layer has tool 'raster', only images
within layers with tool 'raster' are treated


### Changed
- no need to select "Laser" tab anymore
- remove "orientation points" creation warning
- change max power for raster
- change .nc extension to .gcode
- better transformation support: use matrix transform instead of translate..

## [0.05] - 2017-09-11
### Changed
- fix scale problem width inkscale 0.92

## [0.04] - 2017-05
### Changed
- Add RGBA support for raster

## [0.03] - 2016-01
Various changes in Raster2Gcode
- Path optimization 
- Bug fix auround the image's number of pixel
- Add fixed parameters of better lisibility
- Add Comments on Raster gcode output for easy debugging

## [0.02.2] - 2014-11
Add Raster2Gcode
